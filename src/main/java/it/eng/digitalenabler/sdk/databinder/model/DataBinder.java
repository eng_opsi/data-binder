package it.eng.digitalenabler.sdk.databinder.model;

/**
 *	This interface provide methods to implement a serializator/deserializator. 
 */
public interface DataBinder {
	
	public void init();
	
	/**
	 * @param entity of type T
	 * @return the entity T serialized in json string
	 */
	public <T> String toJson(T entity);
	
	/**
	 * 
	 * @param json string
	 * @return the corresponding Object
	 */
	public <T> T toEntity(String json, Class<T> clazz);
}
